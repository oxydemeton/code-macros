import sys

class macro:
    name : str
    code : str

    def __init__(self, name : str, content : str):
        self.name = name
        self.code = content
class aFile:
    rem_def :bool = False
    path : str
    def __init__(self, path : str):
        self.path = path
        self.rem = False
    def __init__(self, path :str, rem) -> None:
        self.path = path
        self.rem_def = rem   

macros = []
def read_table(path : str):
    global macros
    with open(path, "r") as f:
        lines = f.readlines()
        for i in lines:
            if i.find(";") > 0 and not i.find("%%%") >= 0:
                macros.append(macro(i[:i.find(";")].replace(" ", ""), i[i.find(";") + 2 :].replace("\n", "")))


def applie(af : aFile, outfile :str):

    with open(af.path, "r") as f:
        lines = f.readlines()

    l = 0
    for l in range(len(lines)):
        if af.rem_def:
            lines[l] = lines[l][:lines[l].find("%%%")] + "\n"

        for m in macros:
            lines[l] = lines[l].replace("?"+m.name, m.code)
                
    with open(outfile, "w") as f:
        f.writelines(lines)

def dynamic_file(path :str, rem :bool):
    with open(path, "r") as f:
        lines = f.readlines()
    global macros
    for l in lines:
        if len(l) >= 6:
            if l.find(";") > 0 and l.find("%%%") >= 0:
                macros.append(macro(l[l.find("%%%"):l.find(";")].replace(" ", "").replace("%%%", ""), l[l.find(";") + 2 :].replace("\n", "")))
                if rem:
                    l = l[:l.find("%%%")]
 


if __name__ == "__main__":
    # 0 = Include 1 = inputfile 2 = outputfile 3 = dynamic
    mode = 3
    output = False
    applie_file : aFile = []
    output_file : str =  []
    dynamic_remove = False

    for i in range(1, len(sys.argv)):
        if sys.argv[i][0] == "-":
            if sys.argv[i] == "-I":
                mode = 0
            elif sys.argv[i] == "-a":
                mode = 1
            elif sys.argv[i] == "-o":
                mode = 2
            elif sys.argv[i] == "-p":
                output = True
            elif sys.argv[i] == "-d":
                mode = 3
            elif sys.argv[i] == "-r":
                dynamic_remove = True
            else:
                print(sys.argv[i] + " Unkown Argument")
                exit(1)
        else:
            if mode == 0:
                read_table(sys.argv[i])
            elif mode == 1:
                dynamic_remove = False
                applie_file.append(aFile(sys.argv[i], dynamic_remove))
            elif mode == 2:
                output_file.append(sys.argv[i])
            elif mode == 3:
                dynamic_file(sys.argv[i], dynamic_remove)
                applie_file.append(aFile(sys.argv[i], dynamic_remove))
                dynamic_remove = False
            else:
                print("[ERROR] UNREACHABLE. 0 > Mode < 3 impossible")
                exit(1)

    if output:
        for i in macros:
            print("Macro " + i.name + ": " + i.code)

    if len(output_file) > 0 and len(output_file) != len(applie_file):
        print("[ERROR] Count of output files and apllie files doesn't match.")
        exit(1)


    for i in range(len(applie_file)):
        if len(output_file) > 0:
            applie(applie_file[i], output_file[i].path)
        else:
            applie(applie_file[i], applie_file[i].path)

