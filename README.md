# Universal Macros
### Purpose
The sript adds some kind of macros to every file you want.
Many languages have macros in their compilers like c but many other languages not.
It also supports every other filetype. So you can use it for example with .txt files to.

### [Examples](https://gitlab.com/oxydemeton/unimacros/-/tree/main/examples)

## How to use

### Create a macro list
###### Example file: [todo.macros](https://gitlab.com/oxydemeton/unimacros/-/blob/main/examples/todo.macros)
1. Create a text file (has not to be .txt file)
2. Write a new macro like following in each one line:
3. Write the name of the macro. (Don't use `;`, spaces or `?`)
4. End the name with `; `. Don't forget the space.
5. Write the code which should replace the name in the rest of the line.
6. If you want comments use `%%%` `anywhere` in the line.

### Use a macro in another file
###### Example file:[todo.c](https://gitlab.com/oxydemeton/unimacros/-/blob/main/examples/todo.c)
1. Write `?` + the name of the macro into the file.
2. If you want to replace the macro in the file itself use: ```$ python3 unimacro.py -I [name of the list] -a [name of the file]```
3. If you want the replacement in a new file use: ``` $ python3 unimacro.py -I [name of the list] -a [name of the file] -o [name of the output file] ```

### Use multiple files
#### Use multiple macro lists
- To use multiple ist write them one after another.
``` python3 unimacro.py -I [first list] [second list] .... ```
#### Use multiple file to applie the macros
- Name all the files one after another
``` python3 unimacro.py -I [first list] .... -a [fist file] [second file] .... ```
#### Use multiple output file
- List them in the same order as the input files
``` $ python3 unimacro.py -I [first list] .... -a [first file] [second file] .... -o [first output] [second output] .... ```
- Alternatively. (Still same ammount of in and output files)
``` $ python3 unimacro.py -I [first list] .... -a [first file] -o [first output] -a [second file] -o [second output] ```

#### Dynamic File
###### Example file:[dynamic_todo.c](https://gitlab.com/oxydemeton/unimacros/-/blob/main/examples/dynamic_todo.c)
- Dynamic files contain macro definitions but also get applied them
- To define a file as such use `-d` before entering the filepath
``` $ python3 unimacro.py -d [dynamic file] ```
- You can use `-o` in the same way as with `-a`
``` $ python3 unimacro.py -d [dynamic input file] -o [output file] ```
- If the macro definitions should be removed in the output use `-r` before the `-d`. (For every file you have to chose.)
``` $ python3 unimacro.py -d -r [dynamic file]```
- Macros from external files are also applied to dynaic file and macros from the dynamic file to `-a` files
- You can combine `-d` `-r` `-a` `-o `-I` in any combination.  

##### Define a macro in a dynamic file
- Start defintion with `%%%`(No comment definition anymore)
- Define in the same way as in a external file.
- First the name, followed by `; ` and the code to replace the name with.
