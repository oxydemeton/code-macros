## Examples

### todo.macros
- List of macros
### todo.c
- simple example erogramm with macros

### dynamic_todo.c
- Simple example with macros
- macro definitions in itself

### Makefile
- Applies the macros from the list to the .c file
- Compiles the c file
